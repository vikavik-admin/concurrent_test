#Concurrent test

https://open-jira.nrao.edu/browse/SSA-6551

setup based on: https://realpython.com/dockerizing-flask-with-compose-and-machine-from-localhost-to-the-cloud/

### Run it

`docker-compose up --build -d --scale web=5`

`docker-compose run web /usr/local/bin/python create_db.py`

### Close it

`docker-compose down`

### Restart

^ close it, then run it

### Get the IP of the containers

First get the container ID: `docker ps`

`docker inspect <container id> | grep "IPAddress"`

### Connect to postgres

`docker-compose run postgres psql -h <ip for postges container> -p 5432 -U postgres --password`